import axios from 'axios';
import React, {Component} from 'react';
import {Text, View, FlatList} from 'react-native';
import {ListItem} from 'react-native-elements';

export default class HelloWorldApp extends Component {
  url = 'http://wadaya.rey1024.com/upload/kategori/';
  componentDidMount() {
    axios
      .get('http://mhs.rey1024.com/apibudaya/getListCategory.php')
      .then(res => {
        const categories = res.data;
        console.log(categories);
        this.setState({categories});
      });
  }
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
    };
  }
  keyExtractor = (item, index) => index.toString();
  renderItem = ({item}) => (
    <ListItem
      title={item.nama}
      leftAvatar={{source: {uri: this.url + item.gambar}}}
    />
  );
  render() {
    return (
      <View>
        <View>
          <Text> Kategori Budaya </Text>
        </View>
        <FlatList
          keyExtractor={this.keyExtractor}
          data={this.state.categories}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}
