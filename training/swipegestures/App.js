import React, {Component} from 'react';
import {StyleSheet, SafeAreaView, FlatList} from 'react-native';
import ListItem, {Separator} from './src/ListItem';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    paddingVertical: 20,
  },
});

export default class App extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <FlatList
          data={goods}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <ListItem
              {...item}
              onSwipeFromLeft={() => alert('swiped from left!')}
              onSwipeFromRight={() => alert('pressed right!')}
            />
          )}
          ItemSeparatorComponent={() => <Separator />}
        />
      </SafeAreaView>
    );
  }
}

const goods = [
  {id: '0', text: 'qwer'},
  {id: '1', text: 'asdf'},
  {id: '2', text: 'fdsa'},
  {id: '3', text: 'zxcv'},
  {id: '4', text: 'vcxz'},
];
