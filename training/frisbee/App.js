import React, {Component} from 'react';
import {Text, View} from 'react-native';

export default class HelloWorldApp extends Component {
  state = {
    myState: res,
  };
  render() {
    return (
      <View>
        <Text>{this.state.myState}</Text>
      </View>
    );
  }
}

const Frisbee = require('frisbee');

const api = new Frisbee({
  baseURI: 'https://jsonplaceholder.typicode.com', // optional
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

// const res = api.get('/todos/1');

const res = api
  .get('/todos/1')
  .then(console.log)
  .catch(console.error);

// console.log(res);
