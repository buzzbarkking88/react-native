import React, {Component} from 'react';
import {createAppContainer} from 'react-navigation';
import {
  DrawerNavigator,
  TopTabNavigator,
  BottomTabNavigator,
} from './src/configs/routes';
import {View, StyleSheet} from 'react-native';

const NavPage = createAppContainer(DrawerNavigator);
const TopTabPage = createAppContainer(TopTabNavigator);
const BottomTabPage = createAppContainer(BottomTabNavigator);
export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <NavPage />
        <TopTabPage />
        <BottomTabPage />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 5,
    backgroundColor: '#F5FCFF',
  },
});
