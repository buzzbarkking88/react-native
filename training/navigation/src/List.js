import React, {Component} from 'react';
import {Text, View, Button} from 'react-native';

export default class List extends Component {
  render() {
    return (
      <View>
        <Text> this is list</Text>
        <Button
          onPress={() => this.props.navigation.navigate('Home')}
          title="Go to Home"
        />
      </View>
    );
  }
}
