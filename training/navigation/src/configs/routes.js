import Home from '../Home';
import List from '../List';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {
  createBottomTabNavigator,
  createMaterialTopTabNavigator,
} from 'react-navigation-tabs';

export const DrawerNavigator = createDrawerNavigator({
  Home: Home,
  List: List,
});

export const TopTabNavigator = createMaterialTopTabNavigator({
  Home: Home,
  List: List,
});

export const BottomTabNavigator = createBottomTabNavigator({
  Home: Home,
  List: List,
});
