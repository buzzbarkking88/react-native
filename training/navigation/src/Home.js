import React, {Component} from 'react';
import {Text, View, Button} from 'react-native';

export default class Home extends Component {
  render() {
    return (
      <View>
        <Text> this is home</Text>
        <Button
          onPress={() => this.props.navigation.navigate('List')}
          title="Go to List"
        />
      </View>
    );
  }
}
