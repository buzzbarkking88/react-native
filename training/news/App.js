import React, {Component} from 'react';
import {createAppContainer} from 'react-navigation';
import {TopTabNavigator} from './config/routes';
import {View, StyleSheet} from 'react-native';

const TopTabPage = createAppContainer(TopTabNavigator);
export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TopTabPage />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 5,
    backgroundColor: '#F5FCFF',
  },
});
