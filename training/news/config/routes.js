import React from 'react';
import News from '../components/News';
import Portofolio from '../components/Portofolio';
// import {createBottomTabNavigator} from 'react-navigation-tabs';
// import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';

export const TopTabNavigator = createMaterialBottomTabNavigator(
  {
    News: {
      screen: News,
      navigationOptions: {
        tabBarLabel: 'News',
        tabBarIcon: ({tintColor}) => (
          <Icon name="envelope" size={20} color="#FFFFFF" />
        ),
      },
    },
    Portofolio: {
      screen: Portofolio,
      navigationOptions: {
        tabBarLabel: 'Portofolio',
        tabBarIcon: ({tintColor}) => (
          <Icon name="user" size={20} color="#FFFFFF" />
        ),
      },
    },
  },
  {
    tabBarOptions: {
      showIcon: true,
    },
  },
);
