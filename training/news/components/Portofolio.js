import React, {Component} from 'react';
import {Text, View, Image, StyleSheet, Button} from 'react-native';

export default class Portofolio extends Component {
  render() {
    return (
      <View style={styles.viewStyle}>
        <Image
          style={styles.image}
          source={{
            uri:
              'https://ichef.bbci.co.uk/news/660/cpsprodpb/AD5D/production/_106018344_garf_face.jpg',
          }}
        />
        <Text style={styles.titleText}>Cat </Text>
        <Text style={styles.articleText}>
          The cat (Felis catus) is a small carnivorous mammal.[1][2] It is the
          only domesticated species in the family Felidae and often referred to
          as the domestic cat to distinguish it from wild members of the
          family.[4] The cat is either a house cat or a farm cat, which are
          pets, or a feral cat, which ranges freely and avoids human contact.[5]
          A house cat is valued by humans for companionship and for its ability
          to hunt rodents. About 60 cat breeds are recognized by various cat
          registries.
        </Text>
        <Button
          onPress={() => this.props.navigation.navigate('News')}
          title="Go to News"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    flexDirection: 'column',
    height: 1000,
    padding: 0,
    backgroundColor: '#FFFFFF',
    flex: 0.3,
  },
  baseText: {
    fontFamily: 'Cochin',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    padding: 10,
  },
  articleText: {
    textAlign: 'justify',
    padding: 10,
  },
  image: {width: 420, height: 150, resizeMode: 'stretch'},
});
