import React, {Component} from 'react';
import {Text, View, Image, StyleSheet, Button} from 'react-native';

export default class News extends Component {
  render() {
    return (
      <View style={styles.viewStyle}>
        <Image
          style={styles.image}
          source={{
            uri:
              'https://akcdn.detik.net.id/community/media/visual/2019/09/30/1d477f0b-838e-471c-a00b-626aecec75ad_169.jpeg?w=780&q=90',
          }}
        />
        <Text style={styles.titleText}>
          Jadi Kapolda Papua Lagi, Irjen Paulus Waterpauw Janji Tuntaskan
          Konflik{' '}
        </Text>
        <Text style={styles.articleText}>
          Jakarta - Irjen Paulus Waterpauw resmi menerima jabatan sebagai
          Kapolda Papua. Paulus berjanji segera menuntaskan konflik yang terjadi
          di sejumlah titik di Papua. Namun, pertama-tama, ia akan fokus dalam
          menangani korban kerusuhan.
        </Text>
        <Button
          onPress={() => this.props.navigation.navigate('Portofolio')}
          title="Go to Portofolio"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    flexDirection: 'column',
    height: 1000,
    padding: 0,
    backgroundColor: '#FFFFFF',
    flex: 0.3,
  },
  baseText: {
    fontFamily: 'Cochin',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    padding: 10,
  },
  articleText: {
    textAlign: 'justify',
    padding: 10,
  },
  image: {width: 420, height: 150, resizeMode: 'stretch'},
});
