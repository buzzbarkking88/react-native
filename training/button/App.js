import React from 'react';
import {ThemeProvider, Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

const App = () => {
  return (
    <ThemeProvider>
      <Button icon={<Icon name="music" size={40} color="white" />} />
    </ThemeProvider>
  );
};

export default App;
