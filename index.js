/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import App from './App';
// import App from './training/axios/App';
// import App from './training/button/App';
// import App from './training/category/App';
// import App from './training/frisbee/App';
// import App from './training/listitem/App';
// import App from './training/navigation/App';
import App from './training/news/App';
// import App from './training/swipegestures/App';
// import App from './training/navigation/App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
